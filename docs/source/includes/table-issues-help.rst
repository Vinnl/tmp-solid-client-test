.. list-table::

   * - Solid Community Forum
           If you have questions about working with Solid or just
           want to share what you're working on, visit the `Solid forum
           <https://forum.solidproject.org>`_. The `Solid forum`_ is a
           good place to meet the rest of the community.

   * - Bugs and Feature Requests (Product)
           To report a bug or to request a new product feature, please
           file a ticket via the `Service Desk
           <https://inrupt.atlassian.net/servicedesk>`_.

   * - Bugs and Feature Requests (Documentation)
           To report a documentation bug or make a documentation
           request, please use the feedback
           widget to create a ticket.
